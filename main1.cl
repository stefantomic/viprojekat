;;;;;; unos dimenzija

(defun enter-dimension()
    (setq dimenzija (read))
    (setq current-state (draw-map dimenzija dimenzija))
  )


;;;;;;;;; crtanje oks igraca
(defun draw-ox(dimenzija)
                 (if(zerop dimenzija) '()
                   (append '(O) (draw-ox(- dimenzija 1)))
                   ) 
  )
;;;;;;;;; crtanje iks igraca
(defun draw-x(dimenzija)
                 (if(zerop dimenzija) '()
                   (append '(X) (draw-x(- dimenzija 1)))
                   ) 
  )

;;;;;;;;; crtanje praznog polja
(defun draw-empty(dimenzija)
                 (if(zerop dimenzija) '()
                   (append '(_) (draw-empty(- dimenzija 1)))
                   ) 
  )

;stampaj-brojke
(defun stampaj-brojke(n first)
  (cond
   ((equalp (+ n 1) first) (format t " "))
   ((zerop first) (prog1(format t "- ")(stampaj-brojke n (+ 1 first))))
   (t (prog2(format t "~a " first)(stampaj-brojke n (+ 1 first))) )
   )  
  )

;;;;;;; stampanje jedne liste matrice
(defun stampaj-listu-matrice(first lista)
  (format t "a%" (car lista) (format t "~a" (code-char(+ first 65))) )
  (if (< first dimenzija) (stampaj-listu-matrice (+ 1 first) (cdr lista)))
  )



;;;;;;;;; iscrtavanje matrice
(defun draw-map(n dimenzija)
  (cond
   ((zerop n) '() )
   ((> n (- dimenzija 2)) (cons(draw-ox dimenzija)(draw-map(- n 1) dimenzija)))
   ((< n 3) (cons(draw-x dimenzija)(draw-map(- n 1) dimenzija)))
   (t (cons(draw-empty dimenzija)(draw-map(- n 1) dimenzija)))
   )
  )


;;;;;;;;;;;;;;;;STAMPAJ
(defun stampaj(state n)
  (if nil state '())
  (stampaj-brojke dimenzija 0) (format t "~%")
  (stampaj-listu-matrice 0 state)

  )

;POTEZ 

(defun potez(x y)
  
  )

(defun pravi-potez(x1 y1 x2 y2 map)
  (setf (nth y1 (nth x1 map)) '_ )
  (setf (nth y2 (nth x2 map)) 'o )
  (setf current-state map)
  (stampaj current-state dimenzija)
  )